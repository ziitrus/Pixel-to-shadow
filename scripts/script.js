
let color_array = [
  {checked:true, value:'#000000'},
  {checked:false, value:'#ffffff'},
  {checked:false, value:'#F44336'},
  {checked:false, value:'#E91E63'},
  {checked:false, value:'#9C27B0'},
  {checked:false, value:'#673AB7'},
  {checked:false, value:'#3F51B5'},
  {checked:false, value:'#2196F3'},
  {checked:false, value:'#03A9F4'},
  {checked:false, value:'#00BCD4'},
  {checked:false, value:'#009688'},
  {checked:false, value:'#4CAF50'},
  {checked:false, value:'#8BC34A'},
  {checked:false, value:'#CDDC39'},
  {checked:false, value:'#FFEB3B'},
  {checked:false, value:'#FFC107'},
  {checked:false, value:'#FF9800'},
  {checked:false, value:'#FF5722'},
  {checked:false, value:'#795548'},
  {checked:false, value:'#607D8B'}
];

function rgbToHex(color) {
  color = "" + color;
  if (!color || color.indexOf("rgb") < 0) {
    return;
  }

  if (color.charAt(0) == "#") {
    return color;
  }

  var nums = /(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(color),
    r = parseInt(nums[2], 10).toString(16),
    g = parseInt(nums[3], 10).toString(16),
    b = parseInt(nums[4], 10).toString(16);

  return "#" + (
    (r.length == 1 ? "0" + r : r) +
    (g.length == 1 ? "0" + g : g) +
    (b.length == 1 ? "0" + b : b)
  );
}

let baseArray = [];
let dataid = 0;
let isDown = false;
let erase = false;
let colorValue;
let r = document.querySelectorAll("[data-repeat]");
let table = document.querySelector("table tbody");
let basePixel = document.querySelector(".basepixel");
let code = document.querySelector(".Code");
let eraseButton = document.querySelector(".erase");
let baseHeight = document.querySelector(".basepixel").clientHeight;
let baseWidth = document.querySelector(".basepixel").clientWidth;
eraseButton.addEventListener("click", (e) => {
  status = false;
  erase = erase ? false : true;
})

table.addEventListener('mousedown', (e) => {
  isDown = true;
  getEvent(e);
});

table.addEventListener('mouseup', () => {
  isDown = false;
});

r = Array.prototype.slice.call(r);

let generate_palette = () => {
  let palette_container = document.querySelector(".palette");
  color_array.forEach(color => {
    let input = document.createElement('div');
    input.type = "radio";
    input.name = "color";
    input.value = color.value;
    input.id = color.value;
    input.checked = color.checked;
    input.addEventListener('click', (e) => {
      colorValue = {
        value: e.target.value
      };
    })
    if(input.checked) {
      colorValue = {
        value: input.value
      };
    }
    input.style.background = color.value;
    input.className = "btn";
    palette_container.appendChild(input);
  });
}

let generate_box_shadow = (array) => {
  let i = 0;
  let x = 0;
  let y = 0;
  let box_shadow = "";
  let box_shadow_pre = "";
  array.map((row, x) => {
    row.map((color, c) => {
      i++;
      if (i != dataid) {
        box_shadow += `${color.y}px ${color.x}px 0px ${color.color},`;
        box_shadow_pre += `${color.y}px ${color.x}px 0px ${color.color},`;
      } else {
        box_shadow += `${color.y}px ${color.x}px 0px ${color.color}`;
        box_shadow_pre += `${color.y}px ${color.x}px 0px ${color.color};`;
      };
    })
  })
  box_shadow_pre = `.pixelart {
    height:${baseHeight}px;
    width:${baseWidth}px;
    background:${array[0][0].color};
    box-shadow:${box_shadow_pre}
  }`;
  basePixel.style.boxShadow = box_shadow;
  code.innerHTML = box_shadow_pre;
}

let generate_color_array = (x, y, color) => {
  let w = basePixel.clientWidth;
  let h = basePixel.clientHeight;
  if (x != 0 || y != 0) {
    baseArray[x][y] = {
      x: x * w,
      y: y * h,
      color: color
    };
  } else {
    baseArray[x][y] = {
      x: x,
      y: y,
      color: color
    };
    basePixel.style.background = color;
  }
  generate_box_shadow(baseArray);
}

let getEvent = (e) => {
  if (isDown) {
    let cases = e.target;
    colorValue = colorValue.value || colorValue;
    cases.style.background = colorValue;
    if (erase) {
      cases.style.background = "transparent";
    }
    let x = cases.getAttribute("data-row");
    let y = cases.getAttribute("data-case");
    generate_color_array(x, y, cases.style.background);
  }
}

let loop_for_construct = (r, it) => {
  document.querySelector("tbody").innerHTML = ""
  return r.map(m => {
    if (it) {
      m.setAttribute("data-repeat", it);
    }
    let iteration = m.getAttribute("data-repeat");
    for (let i = 0; i < iteration; i += 1) {
      if (m.nodeName === "TR") {
        let tr = document.createElement("TR");
        tr.className = "row"
        table.appendChild(tr);
        baseArray.push([]);
        for (let j = 0; j < iteration; j += 1) {
          dataid++;
          let td = document.createElement("TD");
          td.className = "cases"
          tr.appendChild(td);
          td.setAttribute("data-row", i);
          td.setAttribute("data-case", j);
          td.setAttribute("data-id", dataid);
          td.addEventListener('mouseover', getEvent);
          baseArray[i].push({
            x: i,
            y: j,
            color: "transparent"
          });
        }
      }
    }
  });
  dataid = 0;
}

let change_pixel_value = (obj) => {
  loop_for_construct(r, obj.value)
}

loop_for_construct(r)
generate_palette()
